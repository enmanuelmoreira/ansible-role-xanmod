variable "project_id" {}
variable "ssh_user" {}
variable "domain" {}

variable "runners" {
  default = "3"
}

variable "region" {
  type    = string
  default = "us-east1"
}

variable "zone" {
  default = "us-east1-b"
}

variable "gcp_credentials_path" {
  default = "~/.gcp/mapanare-net-sa.json"
}

variable "instance_type" {
  default = "n1-standard-2"
}

variable "ci_runner_disk_size" {
  type        = string
  default     = "50"
  description = "The size of the persistent disk in GB."
}
