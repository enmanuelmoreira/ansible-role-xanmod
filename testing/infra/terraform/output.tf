output "runners-ip-addresses" {
  value = google_compute_instance.gitlab-runner-instances[*].network_interface.0.access_config.0.nat_ip
}
