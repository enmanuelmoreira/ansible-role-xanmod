resource "google_compute_firewall" "allow-ssh-port" {
  name    = "default-allow-ssh"
  network = google_compute_network.vpc-gitlab-ci.self_link

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "allow-ingress-from-iap" {
  name    = "allow-ingress-from-iap"
  network = google_compute_network.vpc-gitlab-ci.self_link

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["35.235.240.0/20"]
}

resource "google_compute_firewall" "allow-http-port" {
  name    = "default-allow-http"
  network = google_compute_network.vpc-gitlab-ci.self_link

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "allow-https-port" {
  name    = "default-allow-https"
  network = google_compute_network.vpc-gitlab-ci.self_link

  allow {
    protocol = "tcp"
    ports    = ["443"]
  }

  source_ranges = ["0.0.0.0/0"]
}
