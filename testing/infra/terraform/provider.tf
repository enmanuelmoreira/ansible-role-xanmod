terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.9.0"
    }
  }
}

provider "google" {
  version     = "4.9.0"
  credentials = file(var.gcp_credentials_path)
  project     = var.project_id
  region      = var.region
  zone        = var.zone
}

provider "google-beta" {
  version     = "4.9.0"
  credentials = file(var.gcp_credentials_path)
  project     = var.project_id
  region      = var.region
  zone        = var.zone
}
