terraform {
  backend "gcs" {
    bucket      = "mapanare-net-tf-gitlab-runner"
    prefix      = "gitlab-ci/state"
    credentials = "~/.gcp/mapanare-net-sa.json"
  }
}
