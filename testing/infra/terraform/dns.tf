resource "google_dns_managed_zone" "domain-zone" {
  name        = "mapanare-net-zone"
  dns_name    = "${var.domain}."
  description = "${var.domain} Zone"
  labels = {
    environment = "testing"
  }
}

resource "google_dns_record_set" "runners-recordset" {
  count = var.runners
  name  = "${count.index + 1}-dedicated-runner.${google_dns_managed_zone.domain-zone.dns_name}"
  type  = "A"
  ttl   = 300

  managed_zone = google_dns_managed_zone.domain-zone.name
  rrdatas = [
    "${google_compute_instance.gitlab-runner-instances.*.network_interface.0.access_config.0.nat_ip[count.index]}"
  ]
}
