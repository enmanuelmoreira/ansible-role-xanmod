resource "google_compute_network" "vpc-gitlab-ci" {
  name                    = "vpc-gitlab-ci"
  auto_create_subnetworks = "true"
}
