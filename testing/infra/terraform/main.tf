resource "google_compute_instance" "gitlab-runner-instances" {
  count                     = var.runners
  name                      = "dedicated-runner-${count.index + 1}"
  machine_type              = var.instance_type
  hostname                  = "${count.index + 1}-dedicated-runner.${var.domain}"
  min_cpu_platform          = "Intel Haswell"
  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
      type  = "pd-standard"
      size  = var.ci_runner_disk_size
    }
  }

  labels = {
    environment = "testing"
  }

  # Esta instancia está configurada como Descartable (preemptible)
  # Dura 24 hrs maximo en ejecución. Google arbitrariamente la puede
  # terminar antes. Sale hasta 80% más ecomomico que una Instanca normal
  # Más info https://cloud.google.com/compute/docs/instances/preemptible
  scheduling {
    preemptible       = true
    automatic_restart = false
  }

  network_interface {
    # A default network is created for all GCP projects
    network = google_compute_network.vpc-gitlab-ci.self_link
    access_config {
      nat_ip = element(google_compute_address.runners-ip.*.address, count.index)
    }
  }

  # Activando virtualización en las instancias
  # CPU Minimo: Intel Haswell
  advanced_machine_features {
    enable_nested_virtualization = true
  }

  metadata = {
    ssh-keys = "${var.ssh_user}:${file("~/.ssh/terraform-gcp.pub")}"
  }
}

resource "google_compute_address" "runners-ip" {
  count = var.runners
  name  = "gitlab-runner-ip-address${count.index + 1}"
}
