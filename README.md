# Ansible Role: Xanmod Kernel

This role installs [Xanmod Kernel](https://xanmod.org/) package on any supported host.

## Requirements

None

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    xanmod_version: linux-xanmod-lts
    xanmod_gpgkey_path: https://dl.xanmod.org/gpg.key
    xanmod_keyring: /etc/apt/trusted.gpg.d/xanmod-kernel.gpg

The version of the metapackage:

    xanmod_version: linux-xanmod-lts

The path where the gpgkey of the repository will be downloaded.

    xanmod_gpgkey_path: https://dl.xanmod.org/gpg.key

It's the place which the gpgkey will be stored.

    xanmod_keyring: /etc/apt/trusted.gpg.d/xanmod-kernel.gpg

## Kernel Metapackages available

**DEBIAN AND UBUNTU**

    linux-xanmod [5.15],
    linux-xanmod-edge [5.16],
    linux-xanmod-lts [5.10],
    linux-xanmod-rt-edge [5.15-rt].
    linux-xanmod-tt [5.15-tt]
    linux-xanmod-rt [5.10-rt].

**FEDORA AND REDHAT**

    kernel-xanmod-cacule [5.14-cacule],
    kernel-xanmod-edge [5.16],
    kernel-xanmod-exptl [5.15],
    kernel-xanmod-lts [5.10],
    kernel-xanmod-tt [5.15-tt]
    kernel-xanmod-rt [5.13-rt].

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: xanmod

## License

MIT / BSD
